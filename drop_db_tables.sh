#! /usr/bin/env bash


help()
{
	echo "Для работы скрипты нужно передать имя базы данных."
}

check_bins()
{
	MYSQL=`which mysql`
	if [[ $MYSQL -eq "" ]]
	then
		echo "Не найден бинарник mysql."
		echo "Работа скрипта завершена."
		exit 1
	fi
}

if [ -z "$1" ]
then
	echo "Не указано имя базы данных."
    help
    echo "Работа скрипта завершена."
    exit 1
fi

check_bins

mysql -Nse 'show tables' $1 | while read table; do mysql -e "drop table $table" $1; done